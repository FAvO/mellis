<?php
/*
Mellis
Copyright (C) 2021 Felix v. Oertzen
mellis@von-oertzen-berlin.de

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

require_once("config.php");

$db = new PDO("sqlite:" . $CONFIG['db']);


$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db-> exec("CREATE TABLE IF NOT EXISTS honeypot(
          id INTEGER PRIMARY KEY NOT NULL,
          friendlyname VARCHAR(128) NOT NULL,
          description VARCHAR(512) NOT NULL,
          url VARCHAR(128) NOT NULL,
          creation TIMESTAMP NOT NULL,
          ip VARCHAR(128) NOT NULL,
          host VARCHAR(256) NOT NULL,
          useragent VARCHAR(256) NOT NULL,
          destination VARCHAR(256) NOT NULL)");

$db-> exec("CREATE TABLE IF NOT EXISTS honeytrace(
          trace INTEGER PRIMARY KEY NOT NULL,
          tracer INTEGER NOT NULL,
          effectivetime TIMESTAMP NOT NULL,
          ip VARCHAR(128) NOT NULL,
          host VARCHAR(256) NOT NULL,
          useragent VARCHAR(256) NOT NULL)");


if (!(file_exists(__DIR__ ."/.installed"))) {
  // Mellis is not installed
  rename("htaccess", ".htaccess");
  if (!touch( __DIR__ ."/.installed")){
    die("Please check storage permissions. Missing write permission for PHP!");
  }
}

if ($_SERVER['REQUEST_URI'] == $CONFIG['installationroot'] || $_SERVER['REQUEST_URI'] == $CONFIG['installationroot']."index.php") {
header("location: " . $CONFIG['installationroot'] . "creator.php");
die();
} else {
  $sl = substr($_SERVER['REQUEST_URI'],strpos($_SERVER["REQUEST_URI"],$CONFIG['installationroot'])+ strlen($CONFIG['installationroot']));

  $statement = $db->prepare("SELECT * FROM honeypot WHERE url = ?");
  $statement->bindParam(1, $sl);
  $statement->execute(); $row = $statement->fetch();
  if ($row) {
    $a = gethostbyaddr($_SERVER['REMOTE_ADDR']);
    $datastmt = $db->prepare("INSERT INTO honeytrace (tracer, effectivetime, ip, host, useragent) VALUES (:trcr, :tm, :ip, :hst, :srgnt)");
    $datastmt->bindParam(':trcr', $row['id']);
    $datastmt->bindParam(':tm', date("Y-m-d H:m:s"));
    $datastmt->bindParam(':ip', $_SERVER['REMOTE_ADDR']);
    $datastmt->bindParam(':hst', $a);
    $datastmt->bindParam(':srgnt', $_SERVER['HTTP_USER_AGENT']);
    $datastmt->execute();
    header("Location: " . $row['destination']);
  } else {
    die("Something went wrong.");
  }
}
?>
