[![License](https://img.shields.io/badge/License-GNU%20General%20Public%20License%20v3.0-green.svg?logo=gnu)](https://www.gnu.org/licenses/gpl-3.0.en.html)

![HTML5](https://img.shields.io/badge/uses-HTML5-red.svg?logo=html5&color=e34f26) ![CSS](https://img.shields.io/badge/uses-CSS3-blue.svg?logo=css3&color=1572b6) ![JavaScript](https://img.shields.io/badge/uses-JavaScript-yellow.svg?logo=javascript&color=f7df1e)
# Mellis
This is a quite simple project which allows you to run your electronic honeypot.
You are able to
* create personal links
* set a redirect to another page
* and see which user agent accessed the page on which time.

## requirements
You will need
* a web space with 10MB free storage
* a php instance which is able to deal with PDO
* 10 minutes time.

## installation
Just put these files on your web space and modify the `config.php` document to your needs.

Now you're ready.

# Notice
This project is kind of "proof of concept" to learn something about CSP [CSP](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP), network security and to see which which messengers and browsers prefetch websites and may reveal my IP.
If you do not use `Mellis` for research or testing use in a closed environment, notice that you may need to inform potential users that you are processing personal data in order to stay in compliance to the GDPR.

I am NOT responsible for any abusive use!  

# third party libraries
This project includes
* moment.js written by [JS Foundation and other contributors](https://github.com/moment/moment/) under [MIT License.](https://github.com/moment/moment/blob/develop/LICENSE)
