<?php /*
Mellis
Copyright (C) 2021 Felix v. Oertzen
mellis@von-oertzen-berlin.de

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
require_once("config.php");

function secretcode($code) {?>
  <body style="background: <?php echo $GLOBALS['COLOR_G'];?>>
    <div id="title">That is your secret.<br><span style="font-size:60%;">Please remember or write your secret down. It's essential for creating shortlinks in order to authentificate yourself.</span></div>
    <?php echo $code;?>
  </body>
  <?php die();
}


function footer() {
  //footer generation
  ?>
  <footer><a href="http<?php echo $_SERVER['HTTPS'] ? "s":""; ?>://<?php echo $_SERVER['HTTP_HOST'];?>/">Mellis - honeypot generator</a></footer>
  <?php
}

$db = new PDO("sqlite:" . $CONFIG['db']);

$createhoneypot = isset($_POST['createhoneypot']);
$userauth = false;
$honeypotexistsalready = false;
if ($createhoneypot) {
  sleep(1);
  if (password_verify($_POST['secret'], password_hash($CONFIG['password'], PASSWORD_DEFAULT))) {
    $userauth = true;
    $le = $db->prepare("SELECT id FROM honeypot WHERE url = ?");
    $le->bindParam(1, htmlentities($_POST['url']));
    $le->execute();
    $honeypotexistsalready = $le->fetchAll();

    if (!$honeypotexistsalready){
      $ie = $db->prepare("INSERT INTO honeypot (friendlyname, description, url, creation, ip, host, useragent, destination) VALUES (:frndlynm, :dscrptn, :url, :crtn, :ip, :host, :useragent, :redirecturl)");

      $f = ($_SERVER['REMOTE_HOST'] ? $_SERVER['REMOTE_HOST'] : "no hostname found");
      $g = ("https://" . htmlentities($_POST['redirecturl']));
      $ie->bindParam(':frndlynm', htmlentities($_POST['friendlyname']));
      $ie->bindParam(':dscrptn', htmlentities($_POST['description']));
      $ie->bindParam(':url', htmlentities($_POST['url']));
      $ie->bindParam(':crtn', htmlentities($_POST['creation']));
      $ie->bindParam(':ip', $_SERVER['REMOTE_ADDR']);
      $ie->bindParam(':host', $f);
      $ie->bindParam(':useragent', $_SERVER['HTTP_USER_AGENT']);
      $ie->bindParam(':redirecturl', $g );
      $ie->execute();
    }
  }
}
?><!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
echo "<meta name=\"NEVER\" content=\"" . $NEVER . "\">";
echo "<meta name=\"createhoneypot\" content=\"" . ($createhoneypot ? "true":"false") . "\">";
echo "<meta name=\"honeypotexistsalready\" content=\"" . ($honeypotexistsalready ? "true":"false") . "\">";
echo "<meta name=\"userauth\" content=\"" . ($userauth ? "true":"false") . "\">";

     ?>
    <title>Mellis</title>
    <link href="static/stylesheets/styles.css" rel="stylesheet" type="text/css" />
    <link href="static/stylesheets/snackbar.css" rel="stylesheet" type="text/css" />
    <link href="static/stylesheets/general.css" rel="stylesheet" type="text/css" />
    <script lang="text/javascript" src="static/utils.js"></script>
    <script lang="text/javascript" src="static/creator.js"></script>

  </head>
  <body>
    <h2>Mellis</h2>
    <?php echo $_SERVER['HTTPS'] ? "":"<h3 class='httpserror'>You reached this page without encryption. It's much safer to use encryption in order to create honeypot urls.</h3>"; ?>
    <!-- DIVISION HERE -->
    <div class="container" <?php if (!($userauth && $createhoneypot && !$honeypotexistsalready)) {echo "data-hidden='true'";}?>>
      <div class="row">
        <h3>your honeypot</h3>
      </div>
      <div class="row">
        <div class="col-25">
          <label>Your honeypot is labed as </label>
        </div>
        <div class="col-75">
          <span><?php echo htmlentities($_POST['friendlyname']);?></span>
        </div>
      </div>
      <div class="row">
        <div class="col-25">
          <label>Your description</label>
        </div>
        <div class="col-75">
          <span><?php echo htmlentities($_POST['description']);?></span>
        </div>
      </div>
      <div class="row">
        <div class="col-25">
          <label>The your honeypot link is</label>
        </div>
        <div class="col-75">
           <span>https://<?php echo $_SERVER['HTTP_HOST'];?><?php echo $_CONFIG['installationroot']?>/<?php echo htmlentities($_POST['url']);?></span>
        </div>
      </div>
      <div class="row">
        <div class="col-25">
          <label>The user gets redirected to</label>
        </div>
        <div class="col-75">
           <span><?php echo htmlentities($_POST['redirecturl']);?></span>
        </div>
      </div>
      <div class="row">
        <input type="button" value="copy clipboard" id="copytoclipboard" <?php echo "data-url=\"https://" . $_SERVER['HTTP_HOST'] . $CONFIG['installationroot'] . htmlentities($_POST['url']) . "\"";?>>
      </div>
    </div>
  </div>
<!-- DIVISION HERE -->
    <form id="shortlinkform" class="container" action="<?php echo $_SERVER['SCRIPT_NAME'];?>" method="post">
      <div class="row">
        <h3>create a honeypot link</h3>
      </div>
      <div class="row">
        <div class="col-25">
          <label for="longlink">Friendly Name</label>
        </div>
        <div class="col-75">
          <input type="text" id="friendlyname" required name="friendlyname" placeholder="How you wanna call your honeypot?" value="<?php echo htmlentities($_POST['friendlyname']);?>">
        </div>
      </div>
      <div class="row">
        <div class="col-25">
          <label for="description">Description</label>
        </div>
        <div class="col-75">
          <input type="text" id="description" required name="description" placeholder="Here you can add a short description..." value="<?php echo htmlentities($_POST['description']);?>">
        </div>
      </div>
      <div class="row">
        <div class="col-25">
          <label for="url">honeypot link</label>
        </div>
        <div class="col-75">
          https://<?php echo $_SERVER['HTTP_HOST']; echo $CONFIG['installationroot'];?><input type="text" id="url" required name="url" placeholder="How should we call the link?" value="<?php echo htmlentities($_POST['url']);?>">
        </div>
      </div>
      <div class="row">
        <div class="col-25">
          <label for="redirecturl">redirection URL</label>
        </div>
        <div class="col-75">
          https://<input type="text" id="redirecturl" required name="redirecturl" placeholder="Whereto you want your bear redirect?" value="<?php echo htmlentities($_POST['redirecturl']);?>">
        </div>
      </div>
      <div class="row">
        <div class="col-25">
          <label for="secret">Secret</label>
        </div>
        <div class="col-75">
          <input type="password" id="secret"  name="secret" required placeholder="This secret empowers you to use this service! If you know it..." >
        </div>
      </div>
      <div class="row">
        <input type="hidden" name="createhoneypot" value="yes"/>
        <input type="submit" value="save"/>
      </div>
    </form>
    <form id="loginform" class="container" method="post" action="honeytraces.php">
      <div class="row">
        <div class="col-25">
          <label for="loginsecret">Secret</label>
        </div>
        <div class="col-75">
          <input type="password" id="loginsecret" name="loginsecret" required placeholder="This secret empowers you to use this service! If you know it..."/>
        </div>
      </div>
      <div class="row">
        <input type="submit" name="listhoney" value="see traces"/>
      </div>
    </form>
    <div id="snackbar_copy" class="snackbar">link copied to clipboard</div>
    <div id="snackbar_auth_failed" class="snackbar">You don't know the secret.<br>How dare you?</div>
    <div id="snackbar_link_already_there" class="snackbar">This shortlink already exits.<br>please choose a different one.</div>
    <?php
 footer();?>
  </body>
</html>
